import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
    imports: [
        CommonModule
    ]
})
export class <%= classify(name) %> Module {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: <%= classify(name) %> Module,
            providers: []
        };
    }
}
