import {
    chain,
    mergeWith,
    apply,
    move,
    Rule,
    template,
    url,
    branchAndMerge
} from '@angular-devkit/schematics';
import { normalize } from '@angular-devkit/core';
import { dasherize, classify } from "@angular-devkit/core/src/utils/strings";
import { schemaOptions } from './schema';

const stringUtils = { dasherize, classify };

export default function (options: schemaOptions): Rule {
    // TODO: Validate options and throw SchematicsException if validation fails
    options.path = options.path ? normalize(options.path) : options.path;

    const templateSource = apply(url('./files'), [
        template({
            ...stringUtils,
            ...options
        }),
        move('src/app')
    ]);

    return chain([
        branchAndMerge(chain([
            mergeWith(templateSource)
        ])),
    ]);

}
